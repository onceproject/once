
from django.conf.urls import patterns, include, url
from django.contrib import admin

import todo

from once_project.views import hello


admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'once_project.views.home', name='home'),
    # url(r'^once_project/', include('once_project.foo.urls')),
    #url(r'^polls/', include('polls.urls')),
    url(r'^admin/', include(admin.site.urls)),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    
    # 
    (r"^item_action/(done|delete|onhold)/(\d*)/$", "item_action"),
    
    ('^hello/$', hello),
)
