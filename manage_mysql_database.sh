#!/bin/bash
#
# ONCE project
#
# Authors: stefan.ivic@gmail.com
#
# This script is used to create database and user (with all permissions) for ONCE Django web application.
#
# CHANGE LOG: (add line for each new change)
# 29.11.2012. stefan: Initial script is created
#


EXPECTED_ARGS=0
E_BADARGS=65

# Path do mysql-a
MYSQL=`which mysql`

# kreiranje baze (ako vec ne postoji)
command1="CREATE DATABASE IF NOT EXISTS once_database CHARACTER SET utf8 COLLATE utf8_general_ci;"

# kreiranje usera (ako vec ne postoji) i dodjeljivanje prava na bazu
command2="GRANT ALL ON once_database.* TO once_web@'localhost' IDENTIFIED BY 'sa3saLEsD45ii';"
command3="FLUSH PRIVILEGES;"

# Spajanje komandi u jednu varijablu
SQL="${command1}${command2}${command3}"
 
if [ $# -ne $EXPECTED_ARGS ]
then
  echo "Usage: $0 dbname dbuser dbpass"
  exit $E_BADARGS
fi

# podizanje mysql-a kao root (pitati će root password za lokalni mysql) i pokretanje gore specificiranih komandi
$MYSQL -uroot -p -e "$SQL"