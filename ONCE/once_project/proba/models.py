# models.py
from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.contrib import admin


class LinuxDistribution(models.Model):
    name = models.CharField(max_length=60)
    webpage = models.CharField(max_length=60)
    
class LinuxDistributionAdmin(admin.ModelAdmin):
    list_display = ["name", "webpage"]
    search_fields = ["name"]

admin.site.register(LinuxDistribution,LinuxDistributionAdmin)



class UserProfile(models.Model):
    user = models.OneToOneField(User)
    
    place = models.CharField(max_length=60)
    avatar = models.ImageField(upload_to='uploads/avatars', default='', blank=True)
    biography = models.TextField(default='', blank=True)
    distribution = models.ForeignKey(LinuxDistribution, blank=True, null=True)
    
    def __unicode__(self):
        return self.user.username


def create_user_profile(sender, instance, created, **kwargs):
    """Create the UserProfile when a new User is saved"""
    if created:
        profile = UserProfile()
        profile.user = instance
        profile.save()

post_save.connect(create_user_profile, sender=User)

